<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use App\Repository\FeedbackRepositoryInterface;
use Illuminate\Http\Request;



class FeedbackController extends Controller
{
    private $feedbackRepository;

    public function __construct(FeedbackRepositoryInterface $feedbackRepository)
    {
        $this->feedbackRepository = $feedbackRepository;
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'message' => 'required'
        ]);

        Feedback::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'message' => $request->message,

        ]);

        return response()->json([
            'success' => true
        ], 200);
    }

}
