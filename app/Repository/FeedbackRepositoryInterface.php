<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 12/1/2020
 * Time: 3:33 PM
 */

namespace App\Repository;

use App\Models\Feedback;
use Illuminate\Support\Collection;

interface FeedbackRepositoryInterface
{
    public function all(): Collection;
}